var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var path = require('path');
var app = express();
var routes = require('./routes/index');
Genre = require('./models/genre');
Book = require('./models/book');

mongoose.connect('mongodb://localhost/bookstore', { useMongoClient: true });
var db = mongoose.connection;

app.use(express.static(__dirname+'/client'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/',routes);


//app.all('*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    // res.sendFile(__dirname + "/client/index.html"); // updated to reflect dir structure
//});

app.get('*', function (req, res, next) {
    res.sendFile(path.resolve('client/index.html'));
});

app.listen(process.env.PORT || 3000);
console.log('bookstore Started');

module.exports = app;
