var router = require('express').Router();
var bookCntrl = require('../controllers/books');
var authCntrl = require('../controllers/auth');

router.get('/genres', bookCntrl.getGenres);
router.post('/genres', bookCntrl.addGenre);
router.put('/genres/:id', bookCntrl.updateGenre);
router.delete('/genres/:id', bookCntrl.deleteGenre);

router.get('/books', bookCntrl.getBooks);
router.get('/books/:id', bookCntrl.getBookById);
router.post('/books', bookCntrl.addBook);
router.put('/books/:id', bookCntrl.updateBook);
router.delete('/books/:id', bookCntrl.deleteBook);

module.exports = router;