var express = require('express');
var router  = express.Router();
var authRoute    = require('./auth');
var bookRoute   = require('./books');

router.get('/', function(req, res){
	res.send('Welcome to my Bookstore');
});

router.use('/api', bookRoute);
router.use('/api/auth', authRoute);


module.exports = router;