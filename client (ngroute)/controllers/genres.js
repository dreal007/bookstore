var app = angular.module('myApp.genres', []);

app.controller('GenresCntrl', ['$http', '$scope', '$location', '$routeParams', function($http, $scope, $location, $routeParams){
	console.log('Genres Controller Loaded.....');
	$scope.addGenre = function(){
		console.log($scope.genre);
		$http.post('/api/genres', $scope.genre)
			.then(function(response){
				$location.url('/books');
		},
			function(err){
				console.log(err);
		});
	}

	$scope.getGenres = function(){
		$http.get('/api/genres')
		.then(function(response){
			$scope.genres = response.data;
			console.log($scope.genres);
		},
			function(err){
				console.log(err);
			});
	}
}]);