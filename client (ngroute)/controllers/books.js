var app = angular.module('myApp.books', []);

app.controller('BooksCntrl', ['$scope','$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams){
	console.log('Books controller Loaded....');
	$scope.getBooks = function(){
		$http.get('/api/books')
			.then(function(response){
				$scope.books = response.data;
				console.log($scope.books);
			}, 
				function(err){
					console.log(err);
			});
	}

	$scope.getBook = function(){
		var id = $routeParams.id;
		$http.get('/api/books/'+id)
			.then(function(response){
				$scope.book = response.data;
				console.log($scope.book);
			}, 
				function(err){
					console.log(err);
			});
	}

	$scope.addBook = function(){
		console.log($scope.book);
		$http.post('/api/books', $scope.book)
			.then(function(response){
				//window.location.href='#/books';
				$location.url('/books');
			}, 
				function(err){
					console.log(err);
			});
	}

	$scope.updateBook = function(){
		var id = $routeParams.id;
		$http.put('/api/books/'+id, $scope.book)
			.then(function(response){
				//window.location.href='#/books';
				$location.url('/books');
			}, 
				function(err){
					console.log(err);
			});
	}

	$scope.removeBook = function(id){
		$http.delete('/api/books/'+id)
			.then(function(response){
				//window.location.href='#/books';
				$location.url('/books');
			}, 
				function(err){
					console.log(err);
			});
	}

	$scope.getGenres = function(){
		$http.get('/api/genres')
		.then(function(response){
			$scope.genres = response.data;
			console.log($scope.genres);
		},
			function(err){
				console.log(err);
			});
	}
	
}]);