var myApp = angular.module ('myApp', ['ngRoute','myApp.books', 'myApp.genres']);
myApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
	$routeProvider.when('/' ,{
		controller: 'BooksCntrl',
		templateUrl: 'views/books.html'
	})
	.when('/books', {
		controller: 'BooksCntrl',
		templateUrl: 'views/books.html'
	})
	.when('/books/details/:id', {
		controller: 'BooksCntrl',
		templateUrl: 'views/book_details.html'
	})
	.when('/books/add', {
		controller: 'BooksCntrl',
		templateUrl: 'views/add_book.html'
	})
	.when('/books/edit/:id', {
		controller: 'BooksCntrl',
		templateUrl: 'views/edit_book.html'
	})
	.when('/books/delete/:id', {
		controller: 'BooksCntrl',
		templateUrl: 'views/edit_book.html'
	})
	.when('/genres', {
		controller: 'GenresCntrl',
		templateUrl: 'views/view_genre.html'
	})
	.when('/genres/add', {
		controller: 'GenresCntrl',
		templateUrl: 'views/add_genre.html'
	})
	 .otherwise({
	 	redirectTo:'/'
	 });

	 $locationProvider.html5Mode(true);
}]);