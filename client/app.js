var myApp = angular.module ('myApp', ['ngRoute', 'ui.router','myApp.books', 'myApp.genres']);
myApp.config(['$routeProvider', 
			  '$locationProvider',
			  '$stateProvider', 
			  '$urlRouterProvider',
			   function($routeProvider, $locationProvider, $stateProvider, $urlRouterProvider){

// 	$routeProvider.when('/' ,{
// 		controller: 'BooksCntrl',
// 		templateUrl: 'views/books.html'
// 	})
// 	.when('/books', {
// 		controller: 'BooksCntrl',
// 		templateUrl: 'views/books.html'
// 	})
// 	.when('/books/details/:id', {
// 		controller: 'BooksCntrl',
// 		templateUrl: 'views/book_details.html'
// 	})
// 	.when('/books/add', {
// 		controller: 'BooksCntrl',
// 		templateUrl: 'views/add_book.html'
// 	})
// 	.when('/books/edit/:id', {
// 		controller: 'BooksCntrl',
// 		templateUrl: 'views/edit_book.html'
// 	})
// 	.when('/books/delete/:id', {
// 		controller: 'BooksCntrl',
// 		templateUrl: 'views/edit_book.html'
// 	})
// 	.when('/genres', {
// 		controller: 'GenresCntrl',
// 		templateUrl: 'views/view_genre.html'
// 	})
// 	.when('/genres/add', {
// 		controller: 'GenresCntrl',
// 		templateUrl: 'views/add_genre.html'
// 	})
// 	 .otherwise({
// 	 	redirectTo:'/'
// 	 });

	$urlRouterProvider.otherwise('/home');
	$stateProvider

		.state('home', {
            url: '/home',
            templateUrl: 'partial-home.html'
        })
		
		.state('books', {
			url: '/books',
			templateUrl:'views/books.html',
			controller : 'BooksCntrl'
		})

		.state('books_detail', {
			url: '/books/details/:id',
			templateUrl:'views/book_details.html',
		    controller : 'BooksCntrl'
		})

        .state('books_add', {
            url: '/books/add',
            controller: 'BooksCntrl',
     		templateUrl: 'views/add_book.html'
        })

        .state('books_edit', {
            url: '/books/edit/:id',
            controller: 'BooksCntrl',
     		templateUrl: 'views/edit_book.html'
        })

        .state('books_delete', {
            url: '/books/delete/:id',
            controller: 'BooksCntrl',
     		templateUrl: 'views/edit_book.html'
        })

        .state('genres', {
            url: '/genres',
            controller: 'GenresCntrl',
 			templateUrl: 'views/view_genre.html'
        })

        .state('genres_add', {
            url: '/genres/add',
            controller: 'GenresCntrl',
 			templateUrl: 'views/add_genre.html'
        });

 	 $locationProvider.html5Mode(true);

 }]);

