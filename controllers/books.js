var book = require('../models/book');
var genre = require('../models/genre');

var Controller = {};

Controller.getBooks = function(req, res){
	book.getBooks(function(err, books){		
		if(err){
			throw err;		
		} 		
		res.json(books); 	
	});

}

Controller.getBookById = function(req, res){
 	var id = req.params.id;
 	book.getBookById( id, function(err, book){
		if(err){
			throw err;
		}
		res.json(book); 	
	});
 }

 Controller.addBook = function(req, res){
	var body = req.body;
	console.log(body);
	book.addBook(body, function(err, book){		
		if(err){			
			throw err;		
		}		
		res.json(book);	
	});
 }

 Controller.updateBook = function(req, res){
 	var body = req.body; 
 	var id = req.params.id;
 	book.updateBook(id, body, {}, function(err, book){
		if(err){
 			throw err;
 		}
		res.json(book);
 	});

 }

 Controller.deleteBook = function(req, res){
 	var id = req.params.id;
 	book.deleteBook(id, function(err, book){
 		if(err){
 			throw err;
 		}
 		res.json(book);
 	});
 }

 Controller.getGenres = function(req, res){
 	genre.getGenres(function(err, genres){
 		if(err){
 			throw err;
 		}
 		res.json(genres);
 	});

 }

 Controller.addGenre = function(req, res){
	var body = req.body;
	genre.addGenre(body, function(err, genre){
		if(err){
 			throw err;
		}
 		res.json(genre);
 	});

 }
 	
 Controller.updateGenre = function(req, res){
 	var body = req.body; 
 	var id = req.params.id;
 	genre.updateGenre(id, body, {}, function(err, genre){
 		if(err){
 			throw err;
		}
 		res.json(genre);
	});

 }

 Controller.deleteGenre = function(req, res){
	var id = req.params.id;
 	genre.deleteGenre(id, function(err, genre){
 		if(err){
 			throw err;
 		}
 		res.json(genre);
 	});

 }	

module.exports = Controller;